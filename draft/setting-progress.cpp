#include <EEPROM.h>

// Define the pins used for the switch, buttons, and LED
#define UP_PIN 3
#define DOWN_PIN 4
#define HIGH_PIN 5
#define LOW_PIN 6
#define LED_PIN 7

// Define the addresses in EEPROM where the max and min voltage values are stored
#define MAX_VOLTAGE_ADDRESS 0
#define MIN_VOLTAGE_ADDRESS 4

// Define the maximum and minimum voltage values that can be set
#define MAX_VOLTAGE 35.0
#define MIN_VOLTAGE 0.0

// Define the LED blink intervals
#define SUCCESS_BLINK_INTERVAL 250
#define FAIL_BLINK_INTERVAL 500

// Define the debounce interval for the buttons
#define DEBOUNCE_INTERVAL 50

// Function to check if the switch is in a valid position
bool switchIsValid() {
  return digitalRead(HIGH_PIN) == LOW && digitalRead(LOW_PIN) == LOW;
}

void setVoltage() {
  // Determine which pin the switch has been moved to
  int pin = 0;
  if (digitalRead(HIGH_PIN) == HIGH) {
    pin = HIGH_PIN;
  } else if (digitalRead(LOW_PIN) == HIGH) {
    pin = LOW_PIN;
  } else {
    // The switch is not in a valid position, so exit the function
    return;
  }

  // Read the current max or min voltage value from EEPROM
  float voltage;
  if (pin == HIGH_PIN) {
    EEPROM.get(MAX_VOLTAGE_ADDRESS, voltage);
  } else {
    EEPROM.get(MIN_VOLTAGE_ADDRESS, voltage);
  }

  // Wait for the UP or DOWN button to be pressed
  unsigned long lastDebounceTime = 0;
  while (!switchIsValid()) {
    if (digitalRead(UP_PIN) == HIGH) {
      if (millis() - lastDebounceTime > DEBOUNCE_INTERVAL) {
        lastDebounceTime = millis();
        voltage += 0.1;
        if (voltage > MAX_VOLTAGE) {
          voltage = MAX_VOLTAGE;
        }
      }
    } else if (digitalRead(DOWN_PIN) == HIGH) {
      if (millis() - lastDebounceTime > DEBOUNCE_INTERVAL) {
        lastDebounceTime = millis();
        voltage -= 0.1;
        if (voltage < MIN_VOLTAGE) {
          voltage = MIN_VOLTAGE;
        }
      }
    }
    delay(10);
  }

  // Store the new max or min voltage value in EEPROM
  if (pin == HIGH_PIN) {
    EEPROM.put(MAX_VOLTAGE_ADDRESS, voltage);
  } else {
    EEPROM.put(MIN_VOLTAGE_ADDRESS, voltage);
  }

  // Read the max and min voltage values from EEPROM to verify that they were saved correctly
  float eepromMaxVoltage, eepromMinVoltage;
  EEPROM.get(MAX_VOLTAGE_ADDRESS, eepromMaxVoltage);
  EEPROM.get(MIN_VOLTAGE_ADDRESS, eepromMinVoltage);

  // If the values match, blink the LED twice with a 250ms interval
  if (pin == HIGH_PIN && voltage == eepromMaxVoltage || pin == LOW_PIN && voltage == eepromMinVoltage) {
    digitalWrite(LED_PIN, HIGH);
    delay(SUCCESS_BLINK_INTERVAL);
    digitalWrite(LED_PIN, LOW);
    delay(SUCCESS_BLINK_INTERVAL);
    digitalWrite(LED_PIN, HIGH);
    delay(SUCCESS_BLINK_INTERVAL);
    digitalWrite(LED_PIN, LOW);
  } else { // If the values do not match, blink the LED endlessly with a 500ms interval
    while (true) {
      digitalWrite(LED_PIN, HIGH);
      delay(FAIL_BLINK_INTERVAL);
      digitalWrite(LED_PIN, LOW);
      delay(FAIL_BLINK_INTERVAL);
    }
  }
}

--------------------------------------------

#include <LiquidCrystal.h>

// Define the LCD pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Define the addresses in EEPROM where the max and min voltage values are stored
#define MAX_VOLTAGE_ADDRESS 0
#define MIN_VOLTAGE_ADDRESS 4

// Define the maximum and minimum voltage values that can be set
#define MAX_VOLTAGE 35.0
#define MIN_VOLTAGE 0.0

// Function to read the current voltage value from the analog input
float checkVoltage() {
  int sensorValue = analogRead(A0);
  return (float)sensorValue / 1023.0 * 35.0;
}

// Function to display a progress bar on the second line of the LCD
void displayProgressBar(float voltage) {
  // Calculate the number of characters to fill in the progress bar
  int numChars = map(voltage, 0, 35, 0, 14);

  // Calculate the number of characters to fill before and after the min and max voltages
  int numMinChars = map(MIN_VOLTAGE, 0, 35, 0, 14);
  int numMaxChars = map(MAX_VOLTAGE, 0, 35, 0, 14);

  // Calculate the position of the min and max voltages within the progress bar
  int minPos = map(MIN_VOLTAGE, 0, 35, 0, 14);
  int maxPos = map(MAX_VOLTAGE, 0, 35, 0, 14);

  // Adjust the position of the "-" and "+" symbols based on the min and max voltages
  if (minPos < numChars) {
    minPos = numChars;
  }
  if (maxPos > numChars) {
    maxPos = numChars;
  }

  // Clear the second line of the LCD and move the cursor to the beginning
  lcd.setCursor(0, 1);
  lcd.print("                ");

  // Write the progress bar to the LCD
  lcd.setCursor(0, 1);
  for (int i = 0; i < 16; i++) {
    if (i == minPos) {
      lcd.print("-");
    } else if (i == maxPos) {
      lcd.print("+");
    } else if (i < numChars) {
      lcd.print("=");
    } else {
      lcd.print(" ");
    }
  }
}

void setup() {
  // Initialize the LCD
  lcd.begin(16, 2);
  lcd.print("Hello, world!");

  // Initialize the EEPROM with default max and min voltage values if they are not set yet
  float maxVoltage, minVoltage;
  EEPROM.get(MAX_VOLTAGE_ADDRESS, maxVoltage);
  EEPROM.get(MIN_VOLTAGE_ADDRESS, minVoltage);
  if (maxVoltage == 0.0) {
    EEPROM.put(MAX_VOLTAGE_ADDRESS, MAX_VOLTAGE);
  }
  if (minVoltage == 0.0) {
    EEPROM.put(MIN_VOLTAGE_ADDRESS, MIN_VOLTAGE);
  }
}

void loop() {
  // Read the current voltage value and display it on the progress bar
  float voltage = checkVoltage();
  displayProgressBar(voltage);

  // Delay for a short period of time to avoid flickering
  delay(50);
}