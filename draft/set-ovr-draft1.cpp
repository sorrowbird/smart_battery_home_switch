const int settingsButtonPin = 2;
const int overrideButtonPin = 3;
bool settingsMode = false;
bool overrideMode = false;

void setup() {
  pinMode(settingsButtonPin, INPUT);
  pinMode(overrideButtonPin, INPUT);
}

void loop() {
  // Check if settings button is pressed
  if (digitalRead(settingsButtonPin) == HIGH) {
    // Enter settings mode
    settingsMode = true;
    overrideMode = false;
    currentMenuItem = 0; // Start with the first menu item
    displayMenuItem();
    while (digitalRead(settingsButtonPin) == HIGH)
      ; // Wait for button to be released
  } else if (settingsMode) {
    // Exit settings mode
    settingsMode = false;
    currentMenuItem = -1; // Clear menu item
    clearDisplay();
  }

  // Check if override button is pressed
  if (digitalRead(overrideButtonPin) == HIGH) {
    // Enter override mode
    overrideMode = true;
    settingsMode = false;
    while (digitalRead(overrideButtonPin) == HIGH)
      ; // Wait for button to be released
  } else if (overrideMode) {
    // Exit override mode
    overrideMode = false;
  }

  // TODO: Implement normal operation behavior
}



// double checkVoltage()
// {
//   volatile double voltageSum = 0.0;
//   // Read ADC samples and calculate average voltage
//   for (int i = 0; i < numSamples; i++)
//   {
//     voltageSum += analogRead(BATTERY_PIN) * (5.0 / 1023.0) * voltageDividerFactor; // Convert ADC value to voltage and scale down by voltage divider factor
//     delay(2);                                            // Wait for next sample
//   }
//   double averageVoltage = voltageSum / numSamples;

//   // Return average voltage as a ~float~ with one decimal place
//   return (double)((int)(averageVoltage * 10)) / 10.0;
// }

// float checkVoltage() {
//   volatile float voltageSum = 0.0;
//   // Read ADC samples and calculate average voltage
//   for (int i = 0; i < numSamples; i++) {
//     voltageSum += analogRead(BATTERY_PIN) *
//                   (5.0 / 1023.0);                // Convert ADC value to voltage
//     delayMicroseconds(sampleInterval * 1000000); // Wait for next sample
//   }
//   float averageVoltage = voltageSum / numSamples;
//   voltageSum = 0.0;

//   // Return average voltage as a float with one decimal place
//   return (float)((int)(averageVoltage * 10)) / 10.0;
// }
