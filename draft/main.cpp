#include <Arduino.h>
#include <EEPROM.h>
#include <LiquidCrystalIO.h>
// When using the I2C version, these two extra includes are always needed. Doing
// this reduces the memory slightly for users that are not using I2C.
#include <IoAbstractionWire.h>
#include <Wire.h>

#define VBAT_MIN_ADDR 0 // Address of the vbat value in EEPROM
#define VBAT_MAX_ADDR 4
#define HOUR_START 8
#define HOUR_END 10

// Setting Index
// 1 - V minimum
// 2 - V maximum
// 3 - Start Hour
// 4 - End Hour

// LCD setup
LiquidCrystalI2C_RS_EN(lcd, 0x27, true);
float initialValue = 0.50;  // Set the initial value to 3.14
bool isInitialized = false; // Flag to keep track of whether the EEPROM has been
                            // initialized Menu options
const char *menuOptions[] = {"V.bat.min", "V.bat.max", "Hour start",
                             "Hour stop"};

void displayMenu(void);
void displayMenu(int MenuPage);
void welcomeSequence();
// void settings();
void setting_list();
void watch_hold_enter();

// Current selection
int currentSelection = 0;

// Vbat value
float vbat = 0.0;
int buttonState = 0;
bool SETTINGS_FLAG = false;
bool SETTINGS_LIST_FLAG = false;
// Button pins
const int ESC_PIN = 8;
const int ENTER_PIN = 9;
const int UP_PIN = 10;
const int DOWN_PIN = 7;
int startTime;

void setup()
{
  // Check if the EEPROM has already been initialized
  EEPROM.get(100, isInitialized);
  if (!isInitialized)
  {
    // Initialize the EEPROM with the initial value
    EEPROM.put(0, initialValue);
    // Set the flag to indicate that the EEPROM has been initialized
    isInitialized = true;
    EEPROM.put(100, isInitialized);
  }
  welcomeSequence();
  // Display the menu
  displayMenu(1);
}

void loop()
{
  // Update currentSelection based on user input
  // // Display the updated menu
  // if (digitalRead(ENTER_PIN) == HIGH && (millis() - starttime) < 4000) {
  //   lcd.clear();
  //   lcd.setCursor(0, 0);
  //   lcd.print("Hold Enter Subroutine");
  //   delay(5000);
  // }
  //
  // setting_list(); //TODO: in favor of Settings, setting_list will show a
  // number to choose the setting option
  // settings(); // Checks settings flag
  // INCOMPLETE: Implement settings scrolling page from 1 - 4 with
  // displayMenu()
  watch_hold_enter();
  delay(200);
}
void displayMenu(int MenuPage)
{
  // Clear the screen
  lcd.clear();
  // Display the menu options
  lcd.setCursor(0, 0);
  switch (MenuPage)
  {
  case 1:
    lcd.print(menuOptions[0]);
    // Print the vbat value below the menu
    lcd.setCursor(0, 1);
    lcd.print("Vbat.min: ");
    lcd.print(vbat, 2); // Display with 2 decimal places
    break;
  case 2:
    // TODO: Vmax Implement
    lcd.print(menuOptions[1]);
    // Print the vbat value below the menu
    lcd.setCursor(0, 1);
    lcd.print("Vbat.max: ");
    lcd.print(vbat, 2); // Display with 2 decimal places
    break;
  case 3:
    // TODO: Hour of sunrise
    lcd.print(menuOptions[2]);
    // Print the vbat value below the menu
    lcd.setCursor(0, 1);
    lcd.print("Start Hour: ");
    lcd.print(vbat, 2); // Display with 2 decimal places
    break;
  case 4:
    // TODO: Hour of sunset
    lcd.print(menuOptions[3]);
    // Print the vbat value below the menu
    lcd.setCursor(0, 1);
    lcd.print("Stop hour: ");
    lcd.print(vbat, 2); // Display with 2 decimal places
    break;
  case 5: // TODO: This will be the statically put dynamically Updated Display
          // Page with, Time: ${time} \n Battery: ${voltage}
    break;
  default:
    // TODO: Implement Default Screen Showing Current Time Along with Battery
    // voltage and UTI\SUN indication
    break;
  }
  // Highlight the current selection
  lcd.setCursor(15, currentSelection);
  lcd.print(">");
}
//
// void settings(int inx) {
//   /*TODO: Immediate Deprecation in favor of event driven development and
//    * numerical menu to go to certain setting and change accordingly*/
//   float vbat_min = EEPROM.read(VBAT_MIN_ADDR);
//   float vbat_max = EEPROM.read(VBAT_MAX_ADDR);
//   int start_hour = EEPROM.read(HOUR_START);
//   int stop_hour = EEPROM.read(HOUR_END);
//   // Read user input
//   bool escPressed = digitalRead(ESC_PIN) == HIGH;
//   bool enterPressed = digitalRead(ENTER_PIN) == HIGH;
//   bool upPressed = digitalRead(UP_PIN) == HIGH;
//   bool downPressed = digitalRead(DOWN_PIN) == HIGH;
//   switch (inx) {
//   case 1:
//     if (escPressed) {
//       currentSelection = 0;
//     } else if (enterPressed) {
//       // Save the vbat value to EEPROM
//       EEPROM.put(VBAT_MIN_ADDR, vbat_min);
//       lcd.setCursor(0, 1);
//       lcd.print("Content Stored!");
//       delay(5000);
//       lcd.clear();
//       displayMenu(1);
//
//     } else if (upPressed) {
//       vbat_min += 0.1;
//       displayMenu(1);
//     } else if (downPressed) {
//       vbat_max -= 0.1;
//       displayMenu(1);
//     }
//     break;
//   case 2:
//     if (escPressed) {
//       currentSelection = 0;
//     } else if (enterPressed) {
//       // Save the vbat value to EEPROM
//       EEPROM.put(VBAT_MAX_ADDR, vbat_max);
//       lcd.setCursor(0, 1);
//       lcd.print("Content Stored!");
//       delay(5000);
//       lcd.clear();
//       displayMenu(2);
//
//     } else if (upPressed) {
//       vbat_max += 0.1;
//       displayMenu(2);
//     } else if (downPressed) {
//       vbat_max -= 0.1;
//       displayMenu(2);
//     }
//     break;
//   case 3:
//     if (escPressed) {
//       currentSelection = 0;
//     } else if (enterPressed) {
//       // Save the vbat value to EEPROM
//       EEPROM.put(HOUR_START, start_hour);
//       lcd.setCursor(0, 1);
//       lcd.print("Content Stored!");
//       delay(5000);
//       lcd.clear();
//       displayMenu(3);
//
//     } else if (upPressed) {
//       vbat += 0.1;
//       displayMenu(3);
//     } else if (downPressed) {
//       vbat -= 0.1;
//       displayMenu(3);
//     }
//     break;
//   case 4:
//     if (escPressed) {
//       currentSelection = 0;
//     } else if (enterPressed) {
//       // Save the vbat value to EEPROM
//       EEPROM.put(HOUR_END, stop_hour);
//       lcd.setCursor(0, 1);
//       lcd.print("Content Stored!");
//       delay(5000);
//       lcd.clear();
//       displayMenu(4);
//
//     } else if (upPressed) {
//       vbat += 1;
//       displayMenu(4);
//     } else if (downPressed) {
//       vbat -= 1;
//       displayMenu(4);
//     }
//     break;
//   }
// }
//
void welcomeSequence()
{
  Wire.begin();
  // EEPROM.get(VBAT_ADDR, vbat);
  //  Initialize the LCD screen
  lcd.begin(16, 2);

  lcd.configureBacklightPin(3);
  lcd.backlight();
  lcd.setCursor(0, 0);
  String message = "TwoBits Workshop";
  for (unsigned int i = 0; i < message.length(); i++)
  {
    lcd.print(message[i]);
    delay(25);
  }
  lcd.setCursor(0, 1);
  lcd.print("Please Wait.");
  delay(1000);
  // Set button pins to input with pull-up
  pinMode(ESC_PIN, INPUT);
  pinMode(ENTER_PIN, INPUT);
  pinMode(UP_PIN, INPUT);
  pinMode(DOWN_PIN, INPUT);
}

void setting_list()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Enter Setting:");
  while (SETTINGS_LIST_FLAG)
  {
    // Check if the up button is pressed
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Enter Setting:");

    if (digitalRead(UP_PIN) == HIGH)
    {
      currentSelection += 1;
      if (currentSelection > 4)
        currentSelection = 0;
      lcd.clear();
      lcd.setCursor(1, 5);
      lcd.print(currentSelection);
    }
    // Check if the down button is pressed
    if (digitalRead(DOWN_PIN) == HIGH)
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Enter Setting:");
      currentSelection -= 1;
      if (currentSelection < 0)
        currentSelection = 0;
      lcd.setCursor(1, 5);
      lcd.print(currentSelection);
    }
    // Check if the esc button is pressed
    if (digitalRead(ESC_PIN) == HIGH)
    {
      SETTINGS_LIST_FLAG = false;
      lcd.clear();
      displayMenu(5);
    }
    // Check if the enter button is pressed
    if (digitalRead(ENTER_PIN) == HIGH)
    {
      SETTINGS_LIST_FLAG = false;
      SETTINGS_FLAG = true;
    }
    delay(500);
  }
}

void watch_hold_enter()
{
  buttonState = digitalRead(ENTER_PIN);
  if (buttonState == HIGH)
  {
    startTime = millis();
    while (buttonState == HIGH && (millis() - startTime) < 4000)
    {
      buttonState = digitalRead(ENTER_PIN);
    }
    if ((millis() - startTime) >= 3000)
    {

      lcd.clear();
      lcd.print("Enter Hold\n subroutine");
      delay(3000);
      SETTINGS_LIST_FLAG = true;
      setting_list();
      SETTINGS_FLAG = true;
      // while (SETTINGS_FLAG)
      //  settings(currentSelection);
    }
  }
}
