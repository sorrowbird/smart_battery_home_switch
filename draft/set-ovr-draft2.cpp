// Include libraries
#include <RTClib.h> // Library for real-time clock module
#include <Wire.h>   // Library for I2C communication

// Define analog input pin for battery voltage
const int batteryPin = A0;

// Define relay pins
const int relayPin = 2;

// Define maximum and minimum voltage thresholds
const float maxVoltage = 12.5; // volts
const float minVoltage = 11.5; // volts

// Create RTC object
RTC_DS1307 rtc;

// Function to check battery voltage and control relay
void checkBatteryVoltage() {
  // Read battery voltage
  float batteryVoltage =
      analogRead(batteryPin) * 0.00488; // Convert analog reading to voltage

  // Print battery voltage to serial monitor
  Serial.print("Battery voltage: ");
  Serial.println(batteryVoltage);

  // Check if battery voltage is above maximum threshold
  if (batteryVoltage > maxVoltage) {
    // Clamp relay into Normally Open position
    digitalWrite(relayPin, HIGH);
    Serial.println("Relay clamped into Normally Open position");
  }
  // Check if battery voltage is below minimum threshold
  else if (batteryVoltage < minVoltage) {
    // Keep relay in Normally Closed position
    digitalWrite(relayPin, LOW);
    Serial.println("Relay kept in Normally Closed position");
  }
  // Check time to determine when to turn on peripherals
  else {
    DateTime now = rtc.now();
    int hour = now.hour();

    // Check if it's time to turn on peripherals
    if (hour >= 8 && hour < 20) {
      // Clamp relay into Normally Open position
      digitalWrite(relayPin, HIGH);
      Serial.println("Relay clamped into Normally Open position");
    } else {
      // Keep relay in Normally Closed position
      digitalWrite(relayPin, LOW);
      Serial.println("Relay kept in Normally Closed position");
    }
  }
}

void setup() {
  // Initialize serial communication
  Serial.begin(9600);

  // Initialize relay pin as output
  pinMode(relayPin, OUTPUT);

  // Initialize real-time clock module
  Wire.begin();
  rtc.begin();

  // Set initial time if necessary
  // rtc.adjust(DateTime(__DATE__, __TIME__));
}

void loop() {
  // Call function to check battery voltage and control relay
  checkBatteryVoltage();

  // Wait for a second before reading voltage again
  delay(1000);
}
