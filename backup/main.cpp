
#pragma GCC optimize("O3")

#include <Arduino.h>
// #include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <time.h>

LiquidCrystal_I2C lcd(PCF8574_ADDR_A21_A11_A01, 4, 5, 6, 16, 11, 12, 13, 14, POSITIVE);

// EEPROM Adresses
#define VBAT_MIN_ADDR 0 // Address of the vbat value in EEPROM
#define VBAT_MAX_ADDR 4
#define HOUR_START 8
#define HOUR_END 10

// Setting Index
// 1 - V minimum
// 2 - V maximum
// 3 - Start Hour
// 4 - End Hour

// Button pins
#define UP_PIN 10
#define DOWN_PIN 7

// maintaenance Buttons
#define SETTINGS_POS 2
#define OVERRIDE_POS 3

bool settingsMode = false;
bool overrideMode = false;

const int numSamples = 10;          // Number of samples to average over
const float sampleInterval = 0.002; // Time interval between samples in seconds

// Analog Pinout for Battery measuring
#define BATTERY_PIN A0

// define relay pin
#define RELAY_PIN 2

// WARNING:
// pins(4,5,6) reserved for respectivly (ce ,sck,io)
// DS1302 rtc;

// SDA A5
// SCL A4

// define Global Variables to use throught the program
// (Initialized Once, Used Everywhere)
// static float maxVoltage = 12.5;         // stored & Retrived from EEPROM Addr 3
static float minVoltage = 11.5;         // stored & Retrived from EEPROM Addr after the maxVoltage's one
const float voltageDividerFactor = 7.0; // Voltage div@i@er factor
volatile double lastVoltage = 0.0;
bool Flag;
// DECLERATIONS: Here is where DECLERATIONS resides
void welcomeSequence();
void routine();
double checkVoltage();
// EEPROM Initialize: this is done once in the device's lifetime to
// initialize the Memory and giving it a start value to avoid NaN issues

// int EEPROM_init();
/*Routine FuncLiquidCrystalI2C_EN_RStion excute endlessly to check for time and and battery voltage*/

void routine();

// END OF DECLERATIONS

// ADC interrupt service routine
ISR(ADC_vect)
{
  // Read ADC result and convert to voltage
  uint16_t adcResult = ADC;
  double voltage = adcResult * (5.0 / 1023.0) * voltageDividerFactor; // Convert ADC value to voltage and scale down by voltage divider factor
  // Store voltage in global variable
  lastVoltage = voltage;
}

void setup()
{ // Do once every power up
  Serial.begin(9600);

  // Set ADC prescaler to 16 for a 1MHz clock speed
  ADCSRA |= (1 << ADPS2);
  // Set ADC reference voltage to AVCC
  ADMUX |= (1 << REFS0);

  welcomeSequence();
}

void loop() { routine(); }

void routine()
{
  float voltage = checkVoltage();

  // if (voltage < minVoltage)
  // {
  //   delay(3000);
  //   if (voltage < minVoltage)
  // }

  lcd.clear();
  lcd.home();

  lcd.print("Volt: " + (String)voltage);
  Serial.println("Volt: " + (String)voltage);
  Serial.print("\n\n\n\n");

  if (voltage < minVoltage)
  {
    lcd.clear();
    lcd.home();
    lcd.print("Undervoltage!");
    lcd.setCursor(0, 1);
    for (int i = 3; i > 0; i--)
    {
      lcd.print("Power down in: ");
      lcd.print(i);
    }
    delay(500);
  }
}
// int EEPROM_init()
// { // initialize once, also with reset button pressed for long
//   if (!EEPROM.read(100))
//   {
//     float maxVoltage = 12.5; // stored & Retrived from EEPROM Addr 3
//     float minVoltage =
//         11.5; // stored & Retrived from EEPROM Addr after the maxVoltage's one
//     EEPROM.write(VBAT_MIN_ADDR, minVoltage);
//     delay(500);
//     EEPROM.write(VBAT_MAX_ADDR, maxVoltage);
//     delay(500);
//     Serial.print("Reset.");
//     EEPROM.write(100, true);
//     if (EEPROM.read(100))
//     {
//       Serial.println("Success");
//       return 100;
//     }
//   }
//   return 0;
// }

double checkVoltage()
{

  int time_start = millis();
  ADCSRA |= (1 << ADIE);                                // Enable ADC interrupt
  ADCSRA |= (1 << ADATE);                               // Enable auto-triggering
  ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Set prescaler to 128
  ADMUX |= (1 << REFS0);                                // Set reference voltage to AVCC
  ADMUX |= (0 << MUX0);                                 // Set input channel to ADC0 (corresponding to pin A0)
  // Enable global interrupts
  sei();

  // Start ADC conversion
  ADCSRA |= (1 << ADSC);

  // Wait for ISR to update lastVoltage
  while (lastVoltage == 0.0);

  int time_taken = millis() - time_start;
  Serial.print("Time: checkVoltage: ");
  Serial.print(time_taken);
  Serial.print(" ms\n");
  // Return lastVoltage as a double with one decimal place
  return (volatile double)(lastVoltage * 10) / 10.0; //TODO: problomatic piece of code...
}

void welcomeSequence()
{
  Wire.begin();
  // EEPROM.get(VBAT_ADDR, vbat);
  //  Initialize the LCD screen
  lcd.begin(16, 2);
  lcd.backlight();
  lcd.setCursor(0, 0);
  String message = "TwoBits Workshop";
  for (unsigned int i = 0; i < message.length(); i++)
  {
    lcd.print(message[i]);
    Serial.print(message[i]);
    delay(25);
  }
  Serial.print('\n');
  lcd.setCursor(0, 1);
  lcd.print("Please Wait.");
  delay(1000);
  // Set button pins to input with pull-up
  pinMode(UP_PIN, INPUT);
  pinMode(DOWN_PIN, INPUT);
}
