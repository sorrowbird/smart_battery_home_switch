
#pragma GCC optimize("O3,Os")

#include <Arduino.h>
// #include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <time.h>

LiquidCrystal_I2C lcd(PCF8574_ADDR_A21_A11_A01, 4, 5, 6, 16, 11, 12, 13, 14, POSITIVE);

// Define the pins used for the switch, buttons, and LED

// Swtich pins
#define UP_PIN 3
#define DOWN_PIN 4

// Buttons pins
#define HIGH_PIN 5
#define LOW_PIN 6

// LED Indicator
#define LED_PIN 7

// **EEPROM**
// Define the addresses in EEPROM where the max and min voltage values are stored
#define MAX_VOLTAGE_ADDRESS 0
#define MIN_VOLTAGE_ADDRESS 4

// Define the maximum and minimum voltage values that can be set
#define MAX_VOLTAGE 26.0
#define MIN_VOLTAGE 21.0

// Define the LED blink intervals
#define SUCCESS_BLINK_INTERVAL 250
#define FAIL_BLINK_INTERVAL 500

// maintaenance Buttons
#define OVERRIDE_POS 8
#define OVERRIDE_LED 9

bool IsRelayClamped;

// define relay pin
#define RELAY_PIN 4

// WARNING:
// pins(4,5,6) reserved for respectivly (ce ,sck,io)
// DS1302 rtc;

// SDA A5
// SCL A4

// define Global Variables to use throught the program
// (Initialized Once, Used Everywhere)
static float maxVoltage = 24.0;         // stored & Retrived from EEPROM Addr 3
static float minVoltage = 23.0;         // stored & Retrived from EEPROM Addr after the maxVoltage's one
const float voltageDividerFactor = 7.0; // Voltage div@i@er factor
volatile float lastVoltage = 0.0;
static float voltageDisplayed = 0.0;

// DECLERATIONS: Here is where DECLERATIONS resides
void welcomeSequence();
void routine();
float checkVoltage();
int observeVoltage();
void displayProgressBar(float voltage);

// EEPROM Initialize: this is done once in the device's lifetime to
// initialize the Memory and giving it a start value to avoid NaN issues
// int EEPROM_init();

// END OF DECLERATIONS

// ADC interrupt service routine
ISR(ADC_vect)
{
  // Read ADC result and convert to voltage
  uint16_t adcResult = ADC;
  double voltage = adcResult * (5.0 / 1023.0) * voltageDividerFactor; // Convert ADC value to voltage and scale down by voltage divider factor
  // Store voltage in global variable
  lastVoltage = voltage;
}

void setup()
{ // Do once every power up
  // Set ADC prescaler to 16 for a 1MHz clock speed
  ADCSRA |= (1 << ADPS2);
  // Set ADC reference voltage to AVCC
  ADMUX |= (1 << REFS0);

  welcomeSequence(); // Pre-setup finished, boot the software, and start the routine
  digitalWrite(OVERRIDE_POS, HIGH);
}

void loop()
{
  routine();
  if (digitalRead(OVERRIDE_POS) == LOW)
  {
    digitalWrite(RELAY_PIN, HIGH);
    digitalWrite(OVERRIDE_LED, HIGH);
  }
  else
  {
    digitalWrite(OVERRIDE_LED, LOW);
  }
}

int observeVoltage() // Make a comparison phase where it only change the content as the voltage change
{
  if (lastVoltage == voltageDisplayed)
    return true;
  else
    return false;
}

void routine() // The routine of software, which excute endlessly (Overriden by Override on pin OVERRIDE_POS)
{
  float voltage = checkVoltage();

  if (voltage > maxVoltage)
  {
    // This will always happen every cycle, check the voltage and turn the relay on.
    digitalWrite(RELAY_PIN, HIGH);
    IsRelayClamped = true;
  }
  displayProgressBar(voltage);
  // displayVoltage(0);  // TODO: deprecated for Progress alternative
  // Serial.println("Volt: " + (String)voltage);
  // Serial.print("\n\n\n\n");

  if (voltage < minVoltage)
  {
    // lcd.clear();
    // lcd.home();
    // lcd.print("Undervoltage!"); Phased Out as debug of routine is no longer needed

    long wait = millis();
    /*replaced a 3 secs, softlocking delay with dynamic one which does the basic things without hanging*/
    while (millis() - wait < 3000)
    {
      voltage = checkVoltage();
      displayProgressBar(voltage);
    }
    // displayVoltage(1); // TODO: deprecated for Progress alternative
    if (voltage < minVoltage)
    {

      while (voltage < maxVoltage)
      {
        voltage = checkVoltage();
        if (digitalRead(OVERRIDE_POS) == LOW)
        {
          digitalWrite(RELAY_PIN, HIGH);
          digitalWrite(OVERRIDE_LED, HIGH);
          IsRelayClamped = false;
        }
        else
        {
          digitalWrite(OVERRIDE_LED, LOW);
          digitalWrite(RELAY_PIN, LOW);
        }
        IsRelayClamped = false;
        displayProgressBar(voltage);
        // displayVoltage(0); // TODO: deprecated for Progress alternative
        if (voltage >= maxVoltage)
          break;
      }
    }
    else
      digitalWrite(RELAY_PIN, HIGH);
    delay(25);
  }
  delay(80);
}

float checkVoltage() // Perfectly Working Function, tested and ready to use
{

  // int time_start = millis();
  ADCSRA |= (1 << ADIE);                                // Enable ADC interrupt
  ADCSRA |= (1 << ADATE);                               // Enable auto-triggering
  ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Set prescaler to 128
  ADMUX |= (1 << REFS0);                                // Set reference voltage to AVCC
  ADMUX |= (0 << MUX0);                                 // Set input channel to ADC0 (corresponding to pin A0)
  // Enable global interrupts
  sei();

  // Start ADC conversion
  ADCSRA |= (1 << ADSC);

  // Wait for ISR to update lastVoltage
  while (lastVoltage == 0.0)
    ;


  return (float)(lastVoltage * 10) / 10.0; // TODO: problomatic piece of code...
}

void displayProgressBar(float voltage)
{
  int numChars = map(voltage, MIN_VOLTAGE, MAX_VOLTAGE, 0, 14);

  int minPos = map(minVoltage, MIN_VOLTAGE, MAX_VOLTAGE, 0, 14);
  int maxPos = map(maxVoltage, MIN_VOLTAGE, MAX_VOLTAGE, 0, 14);

  if (!observeVoltage())
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Volt: " + (String)voltage);

    // Clear the second line of the LCD and move the cursor to the beginning
    lcd.setCursor(0, 1);
    lcd.print("                ");

    // Write the progress bar to the LCD
    lcd.setCursor(0, 1);
    for (int i = 0; i <= 16; i++)
    {
      if (i == 0)
        lcd.print('[');
      if (i == 14)
        lcd.print(']');
      if (i == minPos)
        lcd.print("-");
      else if (i == maxPos && (maxPos != minPos + 1))
        lcd.print("+");
      else if (i < numChars)
        lcd.print("=");
      else
        lcd.print(" ");
    }
    voltageDisplayed = lastVoltage;
  }
}

void welcomeSequence() // main startup
{
  Wire.begin();
  lcd.begin(16, 2);
  lcd.backlight();
  lcd.setCursor(0, 0);
  String message = F("TwoBits Workshop");
  for (unsigned int i = 0; i < message.length(); i++)
  {
    lcd.print(message[i]);
    Serial.print(message[i]);
    delay(10);
  }
  Serial.print('\n');
  lcd.setCursor(0, 1);
  lcd.print("Please Wait.");
  delay(100);

  pinMode(OVERRIDE_POS, INPUT);
  pinMode(OVERRIDE_LED, OUTPUT);

  digitalWrite(OVERRIDE_POS, HIGH);
}
